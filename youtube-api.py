import sys
from re import VERBOSE
from googleapiclient.discovery import build

if len(sys.argv) != 2:
  print()
  print('Missing api_key')
  print('Usage: python3 ' + sys.argv[0] + ' api_key')
  print()
  exit(0)

api_key = sys.argv[1]

youtube = build('youtube', 'v3', developerKey=api_key)

nextPageToken = None
while True:
    pl_request = youtube.playlistItems().list(
        part='contentDetails',
        playlistId="PLEt08BATxbYr73Iu6f-ZcIQ98U_oXuUEJ",
        maxResults=100,
        pageToken=nextPageToken
    )

    pl_response = pl_request.execute()

    vid_ids = []
    for item in pl_response['items']:
        vid_ids.append(item['contentDetails']['videoId'])
        

    vid_request = youtube.videos().list(
        part="snippet",
        id=','.join(vid_ids)
    )

    vid_response = vid_request.execute()

    for item in vid_response['items']:
        vid_published = item['snippet']['publishedAt']
        vid_title = item['snippet']['title']
        # Replacing the description newlines (\n) with '#_#' to have the description fileld in one line and easily convert it back if needed
        vid_desc = item['snippet']['description'].replace('\n', '#_#')         
        vid_id = item['id']
        yt_link = f'https://youtu.be/{vid_id}'
        print(vid_published, ';', vid_title, ';', yt_link, ';"', vid_desc, '"')        

    nextPageToken = pl_response.get('nextPageToken')

    if not nextPageToken:
        break


