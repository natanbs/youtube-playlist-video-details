# Youtube-playlist-video-details

Get the following videos details from a YouTube playlist videos:
- Published date
- Title
- YouTube URL
- Description

## Installation
Get your api key from google console: https://console.cloud.google.com/apis/api/youtube.googleapis.com

To get the api key, you can use this guide: https://www.youtube.com/watch?v=N18czV5tj5o

Install the google api python client
```
 pip install google-api-python-client
```

## Usage
```
python3 youtube-api.py [Your YouTube Api Key]
```
To output to a csv format:
```
python3 youtube-api.py [Your YouTube Api Key] > youtube-playlist.csv

```
